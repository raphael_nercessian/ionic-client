import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule, NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
 


import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ReadPage } from '../pages/read/read';
import { LoginPage } from '../pages/login/login';
import { HttpModule } from '@angular/http';

import { IonicStorageModule } from '@ionic/storage';

 
 

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ReadPage,
    LoginPage
 
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ReadPage,
    LoginPage
 
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    [NavController],
     
 
  ]
})
export class AppModule {}
