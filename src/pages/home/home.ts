import { Component } from '@angular/core';
import { NavController, NavParams}  from 'ionic-angular';
import { ReadPage } from '../read/read';

import { LoginPage } from '../login/login';
import { Http, Headers } from '@angular/http';
 
import { Storage } from '@ionic/storage';
//import { LocationTrackerProvider } from '../../providers/location-tracker/location-tracker';
 
import 'rxjs/add/operator/map';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
//  providers: [LocationTrackerProvider]
})
export class HomePage {
  public data:any;

  constructor(public navCtrl: NavController, public http: Http,  public storage: Storage, public navParams: NavParams)  
  {
   	let header = new Headers();
  	header.append('Accept','application/json');
    header.append('Access-Control-Allow-Origin','*');	
  	header.append('Authorization', 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImFhNjQ3MmMyYzc2ODE2Y2Y5MmFiYmUwZjhkMzNhOTM3ZjkxNDI5ODU4Zjc4ZTcwMmY4MzU0NmYyMGNkMjg3ZDNhYzAzOWI1ZjRmNGMyMmUzIn0.eyJhdWQiOiI1IiwianRpIjoiYWE2NDcyYzJjNzY4MTZjZjkyYWJiZTBmOGQzM2E5MzdmOTE0Mjk4NThmNzhlNzAyZjgzNTQ2ZjIwY2QyODdkM2FjMDM5YjVmNGY0YzIyZTMiLCJpYXQiOjE1MDc2MDU1NTgsIm5iZiI6MTUwNzYwNTU1OCwiZXhwIjoxNTM5MTQxNTU4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.CWgNDhH525dG8x_BdNN21pkRHiCoz67baziBwD-CK0aduwUnO16qyGYfA3WP2Yydrx6NTOHvwEy-RlybPWktdwkuvu1TKbPslhX4yPc2ba75b5x1kj2SZwusiPyZBsXEuabHmRPxikvCWPUq4yz3wJWWJP2lkwOsaMMvQTAjnfGOIwRWQltH3n_pWBFaTxbheoy-LMb8phsN-geU7OMWfDtHJrx8mcZit4DbqTEv_dMcykBePOsUrjrV2hZHmHXrbIaCwflKSls_Q65WIW5viRLjlOtpVlCuHXLHhXAgOc3Md48_J5WJYIGPUhEACBe8E-nVaND3b8bT2z6rhIQI4Ys0eu3PFpuEnRARqOM5YK3UH37VT9TlXa8rKtMG3roWY8Yp8IkTCZsHmhGCuA3BqVCVCNJQu14Nk9WwQXWiNgt683PUAbkvmuBXFPf3d4MxHhpse6czZqQtqtNybgj6CTOWSywelVU6DQfd-M-lAYFuqDC4jBdxLXuFNSiOtMDSR3fz96gUTl56l_g2-fKslslN1bm2ElOw00jmypLKAZK9vA84Jw_5qZ3OMQSjM5T3vu3pR_AgrFMZ71PBvdOcwlc4PfsSZsy6BgTQ2YoK5h0U6CEJGjMOSJjRxN5L5D3EdHtken5LhH7CvkvHBUiMhMh6LQTTQjfQuUXcC7lNoRk');
	
	this.storage.get('key').then((val) => 
	{

            if (val){
  	this.http.get('http://passport.app/api/noticia', {headers: header} ).
  	map( res => res.json())
  	.subscribe(
  		data=>{
  			this.data = data;
  		},
  		err=>{
  			console.log('error');
  		}
  		);
  	} else {
  	 	return this.navCtrl.setRoot(LoginPage, {}, {animate: true, direction: 'forward'});

  	}
  });
	
	 

  }

  	openReadPage(data)
  	{
  		this.navCtrl.push(ReadPage, {data: data});	
  	}
   	logout(){
   		this.storage.remove('key');
   		return this.navCtrl.setRoot(LoginPage, {}, {animate: true, direction: 'forward'});

   	}
 
 

}
