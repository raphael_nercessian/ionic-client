import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ReadPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-read',
  templateUrl: 'read.html',
})
export class ReadPage {
 	 item;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.item = navParams.get('data');
 	 console.log(this.item.id);
 
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReadPage');
  //  console.log(this.item);
  }

}
