import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators} from "@angular/forms";
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import { HomePage } from '../home/home';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
 
})
export class LoginPage 
{

    myForm: FormGroup;
  	 	 
  	estaAutenticado: boolean;
  	email: string;
  	pass: string;

  	constructor(public navCtrl: NavController, public formBuilder: FormBuilder, public http: Http,  public storage: Storage, public navParams: NavParams) {
  	    this.myForm = formBuilder.group({
            'email': ['', Validators.required],
            'pass': ['', Validators.required]
        })


  	}
  	onSubmit(formData) {
 
    console.log('Form data is ', JSON.stringify(formData.value));
		   // var headers = new Headers();
		 // headers.append('Content-Type' , 'application/x-www-form-urlencoded');
		   // headers.append('Accept', 'application/json');	
  		 	//headers.append('Authorization', 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImFhNjQ3MmMyYzc2ODE2Y2Y5MmFiYmUwZjhkMzNhOTM3ZjkxNDI5ODU4Zjc4ZTcwMmY4MzU0NmYyMGNkMjg3ZDNhYzAzOWI1ZjRmNGMyMmUzIn0.eyJhdWQiOiI1IiwianRpIjoiYWE2NDcyYzJjNzY4MTZjZjkyYWJiZTBmOGQzM2E5MzdmOTE0Mjk4NThmNzhlNzAyZjgzNTQ2ZjIwY2QyODdkM2FjMDM5YjVmNGY0YzIyZTMiLCJpYXQiOjE1MDc2MDU1NTgsIm5iZiI6MTUwNzYwNTU1OCwiZXhwIjoxNTM5MTQxNTU4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.CWgNDhH525dG8x_BdNN21pkRHiCoz67baziBwD-CK0aduwUnO16qyGYfA3WP2Yydrx6NTOHvwEy-RlybPWktdwkuvu1TKbPslhX4yPc2ba75b5x1kj2SZwusiPyZBsXEuabHmRPxikvCWPUq4yz3wJWWJP2lkwOsaMMvQTAjnfGOIwRWQltH3n_pWBFaTxbheoy-LMb8phsN-geU7OMWfDtHJrx8mcZit4DbqTEv_dMcykBePOsUrjrV2hZHmHXrbIaCwflKSls_Q65WIW5viRLjlOtpVlCuHXLHhXAgOc3Md48_J5WJYIGPUhEACBe8E-nVaND3b8bT2z6rhIQI4Ys0eu3PFpuEnRARqOM5YK3UH37VT9TlXa8rKtMG3roWY8Yp8IkTCZsHmhGCuA3BqVCVCNJQu14Nk9WwQXWiNgt683PUAbkvmuBXFPf3d4MxHhpse6czZqQtqtNybgj6CTOWSywelVU6DQfd-M-lAYFuqDC4jBdxLXuFNSiOtMDSR3fz96gUTl56l_g2-fKslslN1bm2ElOw00jmypLKAZK9vA84Jw_5qZ3OMQSjM5T3vu3pR_AgrFMZ71PBvdOcwlc4PfsSZsy6BgTQ2YoK5h0U6CEJGjMOSJjRxN5L5D3EdHtken5LhH7CvkvHBUiMhMh6LQTTQjfQuUXcC7lNoRk');

		  //  let options = new RequestOptions({headers: headers});
		this.http.post('http://passport.app/oauth/token', {
		  'client_id': 9,
		  'client_secret': 'BkPjIY4Hb2059KJfLEinRBtDEY3bp8MW4dtw0rH3',
		  'grant_type': 'password',
		  'username': formData.value.email,
		  'password': formData.value.pass
		}).subscribe(data => {
				//token:angular.fromJson(data);
		 this.storage.set('key','1');

		 



			 
 			this.navCtrl.setRoot(HomePage, {}, {animate: true, direction: 'forward'});
		    
 			 var fixed = this.fixJson(data);
		    console.log(fixed);
		    
		      }, error => {
		        console.log(formData.value.email);
		      });


		  }

		   fixJson(input)
			{
			    var output = input;
			    for (var x = 0; x < input.Length; x++)
			    {
			        if (input[x] != '\"') continue;

			        for (var y = x + 1; y < input.Length; y++)
			        {
			            if (input[y] != '\"') continue;

			            var found = false;
			            for (var z = y + 1; z < input.Length; z++)
			            {
			                if (input[z] != '\"') continue;

			                var tmp = input.Substring(y + 1, z - y - 1);
			                if (tmp.Any(t => t != ' ' && t != ':' && t != ',' && t != '{' && t != '}'))
			                {
			                    output = output.Replace("\"" + tmp + "\"", "\\\"" + tmp + "\\\"");
			                }

			                x = y;
			                found = true;
			                break;
			            }

			            if (found)
			                break;
			        }
			    }

			    return output;
			}

 

 

 

}
